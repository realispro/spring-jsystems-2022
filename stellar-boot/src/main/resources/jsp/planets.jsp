<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

    <jsp:include page="header.jsp"/>
    <span><spring:message code="planets.title" arguments="${system.name}"/></span>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>
            <c:forEach items="${planets}" var="planet">
                <tr>
                    <td>${planet.id}</td>
                    <td>${planet.name}</td>
                </tr>
            </c:forEach>

        </tbody>
    </table>


    <jsp:include page="footer.jsp"/>
