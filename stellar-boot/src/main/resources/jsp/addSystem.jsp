<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<jsp:include page="header.jsp"/>

<span>Adding Planetary Systems</span>

<form:form action="./addSystem" method="post" modelAttribute="systemForm">
<table>
    <tbody>
        <tr>
          <td><spring:message code="system.name"/></td>
          <td><form:input path="name"/><form:errors path="name"/> </td>
        </tr>
        <tr>
            <td><spring:message code="system.star"/></td>
            <td><form:input path="star"/><form:errors path="star"/> </td>
        </tr>
        <tr>
            <td><spring:message code="system.distance"/></td>
            <td><form:input path="distance"/><form:errors path="distance"/> </td>
        </tr>
        <tr>
            <td><spring:message code="system.discovery"/></td>
            <td><form:input type="date" path="discovery"/><form:errors path="discovery"/> </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit"></td>
        </tr>
    </tbody>
</table>
</form:form>


<jsp:include page="footer.jsp"/>
