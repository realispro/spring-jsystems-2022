package lab.spring.boot.stellarboot.web;

import lab.spring.boot.stellarboot.dao.PlanetDAO;
import lab.spring.boot.stellarboot.dao.SystemDAO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.logging.Logger;

@Controller
@RequestMapping("/ui")
@AllArgsConstructor
public class StellarController {

    private static Logger logger = Logger.getLogger(StellarController.class.getName());

    private SystemDAO systemDAO;
    private PlanetDAO planetDAO;


    @GetMapping("/systems")
    public String getSystems(Model model){

        logger.info("about to retrieve systems");

        model.addAttribute("systems", systemDAO.findAll());

        return "systems";
    }

}
