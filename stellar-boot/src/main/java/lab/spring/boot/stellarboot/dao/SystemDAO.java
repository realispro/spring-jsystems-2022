package lab.spring.boot.stellarboot.dao;

import lab.spring.boot.stellarboot.model.PlanetarySystem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemDAO extends JpaRepository<PlanetarySystem, Integer> {
}
