package lab.spring.boot.stellarboot.dao;

import lab.spring.boot.stellarboot.model.Planet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlanetDAO extends JpaRepository<Planet, Integer> {
}
