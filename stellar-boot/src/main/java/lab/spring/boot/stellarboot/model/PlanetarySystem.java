package lab.spring.boot.stellarboot.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;

@Entity
@Table(name="planetarysystem")
@Getter
@Setter
//@ToString
@EqualsAndHashCode
//@Data
@NoArgsConstructor
public class PlanetarySystem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "name")
    private String name;

    private String star;

    private Date discovery;

    private float distance;

    private URL details;

    @JsonIgnore
    @OneToMany(mappedBy = "system", fetch = FetchType.EAGER)
    private List<Planet> planets;

    public PlanetarySystem(int id, String name, String star, Date discovery, float distance, String url) {
        this.id = id;
        this.name = name;
        this.star = star;
        this.discovery = discovery;
        this.distance = distance;
        try {
            details = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "PlanetarySystem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", star='" + star + '\'' +
                ", discovery=" + discovery +
                ", distance=" + distance +
                ", details=" + details +
                '}';
    }
}
