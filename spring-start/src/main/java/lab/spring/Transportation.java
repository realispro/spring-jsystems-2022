package lab.spring;

public interface Transportation {

    void transport(Person passenger);
}
