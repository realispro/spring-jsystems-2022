package lab.spring.impl;

import lab.spring.Accomodation;
import lab.spring.Person;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Component
public class Hotel implements Accomodation {

    //@Autowired
    @Resource
    private List<String> meals;

    @Override
    public void host(Person guest) {
        System.out.println("Guest " + guest + " is being hosted in our hotel. Serving meal:" + meals);
    }


}
