package lab.spring.impl;

import lab.spring.Cheap;
import lab.spring.Person;
import lab.spring.Transportation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
//@Primary
@Cheap
public class Bus implements Transportation {

    @PostConstruct
    public void init(){
        System.out.println("bus constructed");
    }


    @Override
    public void transport(Person passenger) {
        System.out.println("passenger " + passenger + " is being transported by bus.");
    }
}
