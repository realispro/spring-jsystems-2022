package lab.spring.impl;

import lab.spring.Person;
import lab.spring.Transportation;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component("transportation")
@Qualifier("fast")
public class Plane implements Transportation {

    @Override
    public void transport(Person passenger) {
        System.out.println("passenger " + passenger + " is being transported by plane");
    }
}
