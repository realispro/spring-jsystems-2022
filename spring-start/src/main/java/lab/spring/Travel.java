package lab.spring;

import org.springframework.stereotype.Component;

@Component
public class Travel {

    private Transportation transportation;
    private Accomodation accomodation;

    //@Autowired
    public Travel(@Cheap Transportation transportation, Accomodation accomodation) {
        this.transportation = transportation;
        this.accomodation = accomodation;
    }

    public void travel(Person traveller){
        System.out.println("Travel started for a person " + traveller);
        transportation.transport(traveller);
        accomodation.host(traveller);
        transportation.transport(traveller);
    }

}
