package lab.spring;

public interface Accomodation {

    void host(Person guest);
}
