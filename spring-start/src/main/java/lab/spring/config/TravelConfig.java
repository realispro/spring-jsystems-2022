package lab.spring.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;

import java.util.List;

@Configuration
@ComponentScan("lab.spring")
@PropertySource("/meal.properties")
@EnableAspectJAutoProxy
public class TravelConfig {

    //@Autowired
    //private Environment env;

    @Value("${custom.meal:owsianka}")
    private String customMeal;

    @Bean
    public List<String> meals(){
        //String customMeal = env.getProperty("custom.meal");
        return List.of(customMeal, "schabowy", "rosolek", "sernik");
    }

}
