package lab.spring.config;

import lab.spring.Person;
import lab.spring.Voucher;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
@Aspect
public class TravelAspect {

    @Before("execution(* lab.spring.impl.*.*(..))")
    public void logEntering(JoinPoint jp){
        System.out.println("[ENTER] " + jp.getTarget().getClass().getName() + ":" + jp.toLongString() );
    }

    @Before("execution(* lab.spring.impl.*.*(lab.spring.Person))")
    public void validateVoucher(JoinPoint jp){
        Person person = (Person) jp.getArgs()[0];
        if(person.getVoucher().getDate().isBefore(LocalDate.now())){
            throw new IllegalStateException("voucher expired: " + person.getVoucher().getDate());
        }
    }

    @Around("execution(public * travel(lab.spring.Person))")
    Object doSomething(ProceedingJoinPoint jp) throws Throwable {

        Object o = null;
        // Before
        Person person = (Person)jp.getArgs()[0];
        if(person.getVoucher().getDate().isBefore(LocalDate.now())){
            System.out.println("[replacing] ticket not valid. Detected at: " + jp.toShortString());
            person.setVoucher(new Voucher(LocalDate.now().plusDays(1)));
        }
        try {
            o = jp.proceed(jp.getArgs());
        } catch (Throwable t) {
            throw t;
        }
        //After

        return o;
    }

}
