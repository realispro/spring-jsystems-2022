package lab.spring;

import lab.spring.config.TravelConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDate;

public class SpringMain {

    public static void main(String[] args) {

        Person person = new Person("Joe", "Doe");
        person.setVoucher(new Voucher(LocalDate.now().minusDays(1)));

        ApplicationContext context = new AnnotationConfigApplicationContext(TravelConfig.class);
                //new ClassPathXmlApplicationContext("classpath:context.xml");

        Travel travel = context.getBean(Travel.class);
        travel.travel(person);
    }

}
