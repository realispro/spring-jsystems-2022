package lab.spring;

import java.time.LocalDate;

public class Voucher {

    private LocalDate date;

    public Voucher(LocalDate date) {
        this.date = date;
    }

    public LocalDate getDate() {
        return date;
    }
}
