package lab.spring.stellar.dao.memory;

import lab.spring.stellar.dao.SystemDAO;
import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
public class InMemorySystemDAO implements SystemDAO {

    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return InMemoryData.systems;
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return InMemoryData.systems.stream().filter(s->s.getName().toLowerCase().contains(like.toLowerCase())).collect(Collectors.toList());
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return InMemoryData.systems.stream().filter(s->s.getId()==id).findFirst().orElse(null);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        int idMax = InMemoryData.systems.stream().sorted((s1, s2)->s2.getId()-s1.getId()).findFirst().get().getId();

        system.setId(++idMax);
        InMemoryData.systems.add(system);
        return system;
    }
}