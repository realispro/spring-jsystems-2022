package lab.spring.stellar.dao.jdbc;

import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class SystemMapper implements RowMapper<PlanetarySystem> {
    @Override
    public PlanetarySystem mapRow(ResultSet rs, int rowNum) throws SQLException {
        PlanetarySystem ps = new PlanetarySystem();
        ps.setId(rs.getInt("system_id"));
        ps.setName(rs.getString("system_name"));
        ps.setDistance(rs.getFloat("system_distance"));
        ps.setDiscovery(rs.getDate("system_discovery"));
        try{
            ps.setDetails(rs.getURL("system_details"));
        } catch (SQLException sql) { // ignore, maybe null
        }
        return ps;
    }
}
