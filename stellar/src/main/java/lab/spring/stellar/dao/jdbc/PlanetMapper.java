package lab.spring.stellar.dao.jdbc;

import lab.spring.stellar.model.Planet;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PlanetMapper implements RowMapper<Planet> {

    @Override
    public Planet mapRow(ResultSet rs, int rowNum) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        return p;
    }
}
