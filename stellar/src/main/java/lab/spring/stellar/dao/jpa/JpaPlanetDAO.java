package lab.spring.stellar.dao.jpa;

import lab.spring.stellar.dao.PlanetDAO;
import lab.spring.stellar.model.Planet;
import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Primary
public class JpaPlanetDAO implements PlanetDAO {

    @PersistenceContext(name = "stellarUnit")
    private EntityManager em;

    @Override
    public List<Planet> getAllPlanets() {
        return em.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        return em.createQuery("select p from Planet p where p.system = :system")
                .setParameter("system", system).getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        return em.createQuery("select p from Planet p where p.name like :name and p.system = :system")
                .setParameter("name", like).getResultList();
    }

    @Override
    public Planet getPlanetById(int id) {
        return em.find(Planet.class, id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        em.persist(p);
        return p;
    }
}
