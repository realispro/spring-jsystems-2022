package lab.spring.stellar.dao.memory;


import lab.spring.stellar.dao.PlanetDAO;
import lab.spring.stellar.model.Planet;
import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Repository
public class InMemoryPlanetDAO implements PlanetDAO {

    Logger logger = Logger.getLogger(InMemoryPlanetDAO.class.getName());

    @Override
    public List<Planet> getAllPlanets() {
        return InMemoryData.systems.stream().map(s->s.getPlanets()).flatMap(List::stream).collect(Collectors.toList());
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        logger.info("fetching planets of " + system );

        return InMemoryData.systems.stream().filter(s->s.equals(system)).findFirst().get().getPlanets();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {

        logger.info("fetching planets of " + system + " using phrase " + like);
        return InMemoryData.systems.stream().filter(s->s.equals(system)).findFirst().get().getPlanets().stream().filter(p->p.getName().contains(like)).collect(Collectors.toList());
    }

    @Override
    public Planet getPlanetById(int id) {
        return getAllPlanets().stream().filter(p->p.getId()==id).findFirst().get();
    }

    @Override
    public Planet addPlanet(Planet p) {
        int idMax = getAllPlanets().stream().sorted((p1,p2)->p2.getId()-p1.getId()).findFirst().get().getId();
        p.setId(++idMax);
        InMemoryData.systems.stream().filter(s->s.getId()==p.getSystem().getId()).findFirst().get().getPlanets().add(p);
        return p;
    }
}
