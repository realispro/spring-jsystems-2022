package lab.spring.stellar.dao.jpa;

import lab.spring.stellar.dao.SystemDAO;
import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
@Primary
public class JpaSystemDAO implements SystemDAO {

    @PersistenceContext(name = "stellarUnit")
    private EntityManager em;

    // SQL -> HQL -> JPQL
    @Override
    public List<PlanetarySystem> getAllPlanetarySystems() {
        return em.createQuery("select s from PlanetarySystem s").getResultList();
    }

    @Override
    public List<PlanetarySystem> getPlanetarySystemsByName(String like) {
        return em.createQuery("select s from PlanetarySystem s where s.name like :name")
                .setParameter("name", like).getResultList();
    }

    @Override
    public PlanetarySystem getPlanetarySystem(int id) {
        return em.find(PlanetarySystem.class, id);
    }

    @Override
    public PlanetarySystem addPlanetarySystem(PlanetarySystem system) {
        em.persist(system);
        return system;
    }
}
