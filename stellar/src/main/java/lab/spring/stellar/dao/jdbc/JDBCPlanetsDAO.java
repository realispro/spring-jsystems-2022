package lab.spring.stellar.dao.jdbc;

import lab.spring.stellar.dao.PlanetDAO;
import lab.spring.stellar.model.Planet;
import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@Repository
public class JDBCPlanetsDAO implements PlanetDAO {

    public static final Logger logger = Logger.getLogger(JDBCPlanetsDAO.class.getName());


    public static final String SELECT_ALL_PLANETS = "select p.id as planet_id, p.name as planet_name " +
            "from planet p";

    public static final String SELECT_PLANETS_BY_SYSTEM = "select p.id as planet_id, p.name as planet_name " +
            "from planet p where system_id=?";

    public static final String SELECT_PLANETS_BY_SYSTEM_AND_NAME = "select p.id as planet_id, p.name as planet_name " +
            "from planet p where system_id=? and name like ?";

    public static final String SELECT_PLANET_BY_ID = "select p.id as planet_id, p.name as planet_name " +
            "from planet p where id=?";


    private JdbcTemplate jdbcTemplate;

    public JDBCPlanetsDAO(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private Planet mapPlanet(ResultSet rs) throws SQLException {
        Planet p = new Planet();
        p.setId(rs.getInt("planet_id"));
        p.setName(rs.getString("planet_name"));
        return p;
    }

    @Override
    public List<Planet> getAllPlanets() {
        /*List<Planet> planets =
                 new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            Statement statement = con.createStatement();) {
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_PLANETS);
            while (resultSet.next()) {
                planets.add(mapPlanet(resultSet));
            }
        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return planets;*/
        return jdbcTemplate.query(SELECT_ALL_PLANETS, new PlanetMapper());
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        /*List<Planet> systems =
                new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_PLANETS_BY_SYSTEM)) {
            prpstm.setInt(1, system.getId());
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapPlanet(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return systems;*/
        return jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM, new PlanetMapper(), system.getId());
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        /*List<Planet> systems = new ArrayList<>();
        try(Connection con = this.dataSource.getConnection();
            PreparedStatement prpstm = con.prepareStatement(SELECT_PLANETS_BY_SYSTEM_AND_NAME)) {
            prpstm.setInt(1, system.getId());
            prpstm.setString(2, "%" + like + "%");
            ResultSet rs = prpstm.executeQuery();
            while (rs.next()) {
                systems.add(mapPlanet(rs));
            }

        } catch (Exception ex) {
            logger.log(Level.SEVERE, ex.getLocalizedMessage(), ex);
        }
        return systems;*/
        return jdbcTemplate.query(SELECT_PLANETS_BY_SYSTEM_AND_NAME, new PlanetMapper(), system.getId(), "%" + like + "%" );
    }

    @Override
    public Planet getPlanetById(int id) {
        return jdbcTemplate.queryForObject(SELECT_PLANET_BY_ID, new PlanetMapper(), id);
    }

    @Override
    public Planet addPlanet(Planet p) {
        return null;
    }
}
