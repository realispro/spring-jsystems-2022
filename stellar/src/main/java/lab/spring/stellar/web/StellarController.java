package lab.spring.stellar.web;

import lab.spring.stellar.model.Planet;
import lab.spring.stellar.model.PlanetarySystem;
import lab.spring.stellar.service.StellarService;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@Controller
public class StellarController {

    private static Logger logger = Logger.getLogger(StellarController.class.getName());

    private StellarService service;

    public StellarController(StellarService service) {
        this.service = service;
    }

    @InitBinder
    void initBinder(WebDataBinder binder){
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(format, false));

        binder.setValidator(new SystemValidator());
    }

    @RequestMapping(path = "/systems", method = RequestMethod.GET)
    public String getSystems(Model model,
                             @RequestParam(value = "phrase", required = false) String phrase,
                             @RequestHeader(value = "User-Agent", required = false) String userAgent,
                             @CookieValue(name="JSESSIONID", required = false) String sessionId){

        logger.info("user-agent: " + userAgent + ", sessionId:" + sessionId);

        List<PlanetarySystem> systems = phrase==null ? service.getSystems() : service.getSystemsByName(phrase);
        model.addAttribute("systems", systems);

        return "systems";
    }


    @GetMapping("/planets")
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId){

        PlanetarySystem system = service.getSystemById(systemId);
        List<Planet> planets = service.getPlanets(system);
        model.addAttribute("planets", planets);
        model.addAttribute("system", system);

        return "planets";
    }

    @GetMapping("/addSystem")
    public String prepareAddSystem(Model model){
        PlanetarySystem system = new PlanetarySystem();
        system.setName("Unknown");
        model.addAttribute("systemForm", system);
        return "addSystem";
    }

    @PostMapping("/addSystem")
    public String addSystem(@ModelAttribute("systemForm") @Validated PlanetarySystem system, BindingResult br){

        if(br.hasErrors()){
            return "addSystem";
        }

        service.addPlanetarySystem(system);

        return "redirect:/systems";

    }

}
