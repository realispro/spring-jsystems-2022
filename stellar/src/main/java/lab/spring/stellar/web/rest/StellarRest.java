package lab.spring.stellar.web.rest;

import lab.spring.stellar.model.Planet;
import lab.spring.stellar.model.PlanetarySystem;
import lab.spring.stellar.service.StellarService;
import lab.spring.stellar.web.SystemValidator;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

@RestController
@RequestMapping("/webapi")
public class StellarRest {

    private static Logger logger = Logger.getLogger(StellarRest.class.getName());

    private StellarService service;

    public StellarRest(StellarService service) {
        this.service = service;
    }

    @InitBinder
    void initBinder(WebDataBinder binder){
        binder.setValidator(new SystemValidator());
    }

    @GetMapping("/systems")
    public List<PlanetarySystem> getSystems(@RequestParam(value = "phrase", required = false) String phrase){
        logger.info("about to provide systems");
        return phrase==null ? service.getSystems() : service.getSystemsByName(phrase);
    }

    @GetMapping("/systems/{systemId}")
    public ResponseEntity getSystemById(@PathVariable("systemId") int systemId){
        logger.info("about to provide system " + systemId);
        PlanetarySystem system = service.getSystemById(systemId);

        if(system!=null){
            return ResponseEntity.status(HttpStatus.OK).body(system);
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("system " + systemId + " not found");
        }
    }

    @GetMapping("/systems/{systemId}/planets")
    public List<Planet> getPlanetsBySystemId(@PathVariable("systemId") int systemId){
        logger.info("about to provide planets for system " + systemId);
        return service.getPlanets(service.getSystemById(systemId));
    }



    @PostMapping("/systems")
    public ResponseEntity addSystem(@RequestBody @Validated PlanetarySystem system, BindingResult br){
        logger.info("about to add new system " + system.getName());

        if(br.hasErrors()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(br.toString());
        }

        return ResponseEntity.status(HttpStatus.CREATED).body(service.addPlanetarySystem(system));
    }

    @GetMapping("/planets/{planetId}")
    public Planet getPlanetById(@PathVariable("planetId") int planetId){
        return service.getPlanetById(planetId);
    }


}
