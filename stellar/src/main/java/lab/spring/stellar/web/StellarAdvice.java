package lab.spring.stellar.web;

import lab.spring.stellar.service.SloganService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class StellarAdvice {

    private SloganService sloganService;

    public StellarAdvice(SloganService sloganService) {
        this.sloganService = sloganService;
    }

    @ModelAttribute
    public void addSlogan(Model model){
        model.addAttribute("slogan", sloganService.getRandomSlogan());
    }

}
