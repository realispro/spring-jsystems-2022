package lab.spring.stellar.web;

import lab.spring.stellar.model.PlanetarySystem;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class SystemValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.isAssignableFrom(PlanetarySystem.class);
    }

    @Override
    public void validate(Object target, Errors errors) {

        PlanetarySystem system = (PlanetarySystem) target;
        if(system.getName()==null||system.getName().trim().isEmpty()){
            errors.rejectValue("name", "error.empty");
        }

        if(system.getDistance()<0){
            errors.rejectValue("distance", "error.negative");
        }
    }
}
