package lab.spring.stellar.web;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalTime;

public class OpeningInterceptor implements HandlerInterceptor {

    private int opening;
    private int closing;

    public OpeningInterceptor(int opening, int closing) {
        this.opening = opening;
        this.closing = closing;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        int currentHour = LocalTime.now().getHour();

        if(currentHour>=opening && currentHour<closing){
            return true;
        } else {
            response.setStatus(418);
            response.sendRedirect(
                    "https://www.istockphoto.com/pl/zdj%C4%99cie/niestety-jeste%C5%9Bmy-zamkni%C4%99t%C4%85-tablic%C4%85-wisi-na-drzwiach-kawiarni-gm1130208883-298835584");
            return false;
        }

    }
}
