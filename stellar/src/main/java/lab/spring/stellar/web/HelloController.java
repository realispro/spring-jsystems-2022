package lab.spring.stellar.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HelloController {

    @RequestMapping(path = "/hello", method = RequestMethod.GET)
    public String hello(Model model){
        model.addAttribute("hello_text", "Wiosna!");
        return "hello";
    }

}
