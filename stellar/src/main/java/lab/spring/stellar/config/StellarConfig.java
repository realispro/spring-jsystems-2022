package lab.spring.stellar.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("lab.spring.stellar")
public class StellarConfig {
}
