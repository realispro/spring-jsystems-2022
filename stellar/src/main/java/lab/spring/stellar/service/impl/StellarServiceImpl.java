package lab.spring.stellar.service.impl;

import lab.spring.stellar.dao.PlanetDAO;
import lab.spring.stellar.dao.SystemDAO;
import lab.spring.stellar.model.Planet;
import lab.spring.stellar.model.PlanetarySystem;
import lab.spring.stellar.service.StellarService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import java.util.List;
import java.util.logging.Logger;

@Service
//@Transactional(propagation = Propagation.REQUIRES_NEW)
public class StellarServiceImpl implements StellarService {

    Logger logger = Logger.getLogger(StellarServiceImpl.class.getName());

    private SystemDAO systemDAO;
    private PlanetDAO planetDAO;

    private PlatformTransactionManager transactionManager;

    public StellarServiceImpl(SystemDAO systemDAO, PlanetDAO planetDAO, PlatformTransactionManager transactionManager) {
        this.systemDAO = systemDAO;
        this.planetDAO = planetDAO;
        this.transactionManager = transactionManager;
    }

    @Override
    public List<PlanetarySystem> getSystems() {
        logger.info("fetching all planetary systems");
        List<PlanetarySystem> systems = systemDAO.getAllPlanetarySystems();
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public List<PlanetarySystem> getSystemsByName(String like) {
        logger.info("fetching planetary systems like " + like);
        List<PlanetarySystem> systems =  systemDAO.getPlanetarySystemsByName(like);
        logger.info("found: " + systems.size());
        return systems;
    }

    @Override
    public PlanetarySystem getSystemById(int id) {
        logger.info("fetching planetary system by id " + id);

        PlanetarySystem system = systemDAO.getPlanetarySystem(id);
        //logger.info("found: " + system);
        return  system;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem s) {
        logger.info("fetching  planets by system " + s);

        List<Planet> planets = planetDAO.getPlanetsBySystem(s);
        logger.info("found:" + planets.size());
        return planets;
    }

    @Override
    public List<Planet> getPlanets(PlanetarySystem system, String like) {
        return planetDAO.getPlanetsBySystemAndName(system,like);
    }


    @Override
    public Planet getPlanetById(int id) {
        return planetDAO.getPlanetById(id);
    }

    @Override
    public Planet addPlanet(Planet p, PlanetarySystem s) {
        p.setSystem(s);
        return planetDAO.addPlanet(p);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED)
    public PlanetarySystem addPlanetarySystem(PlanetarySystem s) {
        //TransactionStatus ts = transactionManager.getTransaction(new DefaultTransactionDefinition());
        try {
            PlanetarySystem system = systemDAO.addPlanetarySystem(s);

            PlanetarySystem system2 = new PlanetarySystem();
            system2.setName(system.getName() + "_bis");
            system2.setStar(system.getStar() + "_bis");
            system2.setDiscovery(system.getDiscovery());
            system2.setDistance(system.getDistance());
            systemDAO.addPlanetarySystem(system2);
            //transactionManager.commit(ts);
            return system;
        }catch (RuntimeException e){
            //transactionManager.rollback(ts);
            throw e;
        }
    }

}
