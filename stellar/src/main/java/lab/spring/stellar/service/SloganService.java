package lab.spring.stellar.service;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class SloganService {

    private static List<String> slogans = List.of(
            "Don't look up!",
            "We are made of stars",
            "The truth is out there"
    );

    public String getRandomSlogan(){
        return slogans.get(new Random().nextInt(slogans.size()));
    }

}
