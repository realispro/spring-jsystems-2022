package lab.spring.stellar.service;



import lab.spring.stellar.config.StellarConfig;
import lab.spring.stellar.model.Planet;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;


public class StellarServiceMain {

    public static void main(String[] args) {
        System.out.println("Let's explore stars!");

        ApplicationContext context = new AnnotationConfigApplicationContext(StellarConfig.class);
        StellarService service = context.getBean(StellarService.class);
        List<Planet> planets = service.getPlanets(service.getSystemById(1));

        planets.forEach(p-> System.out.println(p));

    }
}
