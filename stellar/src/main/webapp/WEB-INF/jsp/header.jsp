<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <title>Planetary Systems</title>
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>"/>
    <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet"/>
</head>
<body>
<header>
    <nav>
        <ul>
            <li class="active"><a
                    href="./systems">Stellar Catalogue</a>
            </li>
        </ul>
        <div>
            <security:authorize access="isAuthenticated()">
                <security:authentication property="principal.username" var="userNameVar"/>
                ${userNameVar}
            </security:authorize>
        </div>
    </nav>
</header>

    <article>
        <jsp:include page="search.jsp"/>
        <section class="data">
